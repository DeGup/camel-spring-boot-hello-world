package com.degup.camel;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class Router extends RouteBuilder {
    public void configure() throws Exception {
        from("timer:foo")
                .log(LoggingLevel.INFO, "Hello World");
    }
}
